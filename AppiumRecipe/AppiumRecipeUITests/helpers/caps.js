const iosCaps = {
  platformName: 'iOS',
  automationName: 'XCUITest',
  deviceName: process.env.IOS_DEVICE_NAME || 'iPhone 11',
  platformVersion: process.env.IOS_PLATFORM_VERSION || '13.3',
  app: undefined
};

const serverConfig = {
  host: process.env.APPIUM_HOST || 'localhost',
  port: process.env.APPIUM_PORT || 4723,
  logLevel: 'info'
};

const iosOptions = Object.assign(
  {
    capabilities: iosCaps
  },
  serverConfig
);

module.exports = {
  iosOptions
};
