import Foundation

protocol HttpBinGateway {    
    func fetch(completion: @escaping (Result<HttpRequestModel,Error>) -> Void)
}
