import SwiftUI
import BasicCleanArch

struct ContentView: View, Displayer {
    typealias ViewModelType = String
    
    @State var result = ""
    @State private var showError = false
    @State private var errorText = ""
    
    var body: some View {
        VStack(alignment: .center) {
            Button(action: startFoundationInteraction) {
                Text("Start")
            }
            Button(action: startAlamofireInteraction) {
                Text("Start with Alamofire")
            }
            Text(result)
        }.alert(isPresented: $showError) { () -> Alert in
            Alert(title: Text("Error"), message: Text(errorText),
                  dismissButton: .cancel(Text("OK")))
        }
    }
    
    func startInteraction(gateway: HttpBinGateway) {
        result = "Started..."
        let ia = GetHttpRequestInteractor(presenter: HttpRequestPresenter(),
                                          gateway: gateway)
        ia.execute(request: nil, displayer: self)
    }

    func startAlamofireInteraction() {
        startInteraction(gateway: HttpBinAlamofireGateway())
    }

    func startFoundationInteraction() {
        startInteraction(gateway: HttpBinFoundationGateway())
    }

    func display(success: String, resultCode: Int) {
        result = success
    }
    
    func display(failure: Error) {
        result = ""
        errorText = failure.localizedDescription
        showError.toggle()
    }
    
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
