import Foundation

class HttpBinFoundationGateway : HttpBinGateway {
    let url = URL(string: "https://httpbin.org/get")!
    let session = URLSession(configuration: .default)
    
    func fetch(completion: @escaping (Result<HttpRequestModel, Error>) -> Void) {
        let task = session.dataTask(with: url) {
            (data, response, error) -> Void in
            guard let data = data else {
                completion(Result.failure(error!))
                return
            }
            do {
                let decoder = JSONDecoder()
                let item = try decoder.decode(HttpRequestModel.self, from: data)
                completion(.success(item))
            } catch  {
                completion(.failure(error))
            }
        }
        task.resume()
    }
    
}

