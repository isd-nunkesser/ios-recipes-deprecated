import Foundation
import Alamofire

class HttpBinAlamofireGateway : HttpBinGateway {
    let url = "https://httpbin.org/get"
    
    func fetch(completion: @escaping (Result<HttpRequestModel,Error>) -> Void) {
        AF.request(url)
            .validate()
            .responseJSON {
                switch $0.result {
                case .success:
                    do {
                        let decoder = JSONDecoder()
                        let item = try decoder.decode(HttpRequestModel.self,
                                                      from: $0.data!)
                        completion(.success(item))
                    } catch {
                        completion(.failure(error))
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
        }
    }
}
